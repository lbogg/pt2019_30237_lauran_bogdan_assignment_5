package data;

import org.joda.time.DateTime;

import java.io.*;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;

import static java.util.stream.Collectors.*;

public class DataHandler {

    private List<MonitoredData> activities;

    private DataHandler()
    {
        activities = new ArrayList<MonitoredData>();
        readd();
    }

    public List<MonitoredData> getActivities() {
        return activities;
    }


    private String timeToString(long sec)
    {
        long min = sec/60;
        long s = sec - min*60;
        long hours = min/60;
        min = min - hours*60;
        return "  "+hours+" hours "+min+" minutes "+s+" seconds";

    }


    private void readd()
    {
        String fileName = "src/Activities.txt";
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
        try (Stream<String> stream = Files.lines(Paths.get(fileName))) {
            stream.forEach(linee-> {
                //line = stream.
                String[] s = linee.split("\t\t");
                LocalDateTime localDateTime = LocalDateTime.parse(s[0],formatter);
                LocalDateTime localDateTime1 = LocalDateTime.parse(s[1],formatter);
                MonitoredData monitoredData = new MonitoredData(s[2], localDateTime, localDateTime1);
                activities.add(monitoredData);});

        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private void printData()
    {
        for (MonitoredData data : activities)
        {
            System.out.println(data.toString());
        }
    }


    private void pb1()
    {
        System.out.println("");
        int nr =0;
        long nrr = activities.stream()
                .map(MonitoredData::getStartTime)
                .map(LocalDateTime::toLocalDate)
                .distinct()
                .count();

        try {
            PrintWriter writer = new PrintWriter("pb1.txt", "UTF-8");
            writer.println( ""+nrr+" distinct days");
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
        System.out.println("TASK 1: "+nrr+" distinct days");
        System.out.println("");
    }

    private void pb2()
    {
        System.out.println("");
        System.out.println("TASK 2:");

        Map<String, Long> my_map = activities.stream()
                .collect(groupingBy(MonitoredData::getActivity, counting()));

        my_map.forEach((key, value)-> System.out.println(key + " : " + value.toString()));
        System.out.println("");

        try {
            PrintWriter writer = new PrintWriter("pb2.txt", "UTF-8");
            my_map.forEach((key, value)-> writer.println(key + " : " + value.toString()));
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void pb3()
    {
        System.out.println("");
        System.out.println("TASK 3:");

        Map<String, Map<String,Long>> my_map = activities.stream()
                .collect(groupingBy(x-> x.getStartTime().toLocalDate().toString(), groupingBy(MonitoredData::getActivity,counting())));

        my_map.forEach((key, mapp)-> mapp.forEach((k,v)-> System.out.println(key + " : " + k +" = "+v)));
        System.out.println("");
        try {
            PrintWriter writer = new PrintWriter("pb3.txt", "UTF-8");
            my_map.forEach((key, mapp)-> mapp.forEach((k,v)-> writer.println(key + " : " + k +" = "+v)));
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void pb44()
    {
        System.out.println("");
        System.out.println("TASK 4.1:");

        activities.forEach(element -> System.out.println(element.toString()+" "+timeToString(element.getDuration().getSeconds())));
    }

    private void pb4()
    {
        System.out.println("");
        System.out.println("TASK 4:");
        DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd");

        Map<String, Long> my_map = activities.stream()
                .collect(groupingBy(MonitoredData::getActivity, summingLong(x->x.getDuration().getSeconds())))
                .entrySet()
                .stream()
                .filter(f -> f.getValue() / 3600 > 10) //sunt 3600 secunde pe ora
                .collect(toMap(Map.Entry::getKey, Map.Entry::getValue));


        my_map.forEach((key,value)-> System.out.println(key + " : "+timeToString(value)));

        System.out.println("");

        try {
            PrintWriter writer = new PrintWriter("pb4.txt", "UTF-8");
            //for (Map.Entry<String,  Long> entry : my_map.entrySet()) {
            //    writer.println(entry.getKey() + " : " + ((entry.getValue())/3600));
            //}
            my_map.forEach((key,value)-> writer.println(key + " : " + timeToString(value)));
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    private void pb5()
    {
        System.out.println("");
        System.out.println("TASK 5:");

        Map<String, Long> my_map = activities.stream()
                .collect(groupingBy(MonitoredData::getActivity, counting()));

        List<String> list = activities.stream()
                .filter( f -> f.getDuration().getSeconds()/60<5)
                .collect(groupingBy(MonitoredData::getActivity, counting()))
                .entrySet()
                .stream()
                .filter(f -> f.getValue() >= 90/100f * my_map.get(f.getKey()) )
                .map(Map.Entry::getKey)
                .collect(toList());

        //System.out.println(""+90/100f);
        list.forEach(System.out::println);

        try {
            PrintWriter writer = new PrintWriter("pb5.txt", "UTF-8");
            //for (String s : list) {
            //    writer.println(s);
            //}
            list.forEach(writer::println);
            writer.close();
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            e.printStackTrace();
        }
    }

    public static void main(String args[])
    {
        DataHandler dataHandler = new DataHandler();
        dataHandler.printData();

        dataHandler.pb1();
        dataHandler.pb2();
        dataHandler.pb3();
        dataHandler.pb4();
        dataHandler.pb5();
        dataHandler.pb44();

    }

}
